from django.urls import path
from accounts.views import login_view, logout, signup
urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout, name='logout'),
    path('signup/', signup, name='signup'), 
]
