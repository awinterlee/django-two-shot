from django.urls import path
from receipts.views import receipt_list, create_receipt, category, accounts, create_category, create_account

urlpatterns = [
    # path('', receipt_list, name='receipt'),
    path('', receipt_list, name='home'),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', category, name='category'),
    path('accounts/', accounts, name='accounts'),
    path('categories/create/', create_category, name='create_category'),
    path('accounts/create/', create_account, name='create_account'),

]
