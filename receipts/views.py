from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm

# Create your views here.
@login_required
def receipt_list(request):
    receipt_objects = Receipt.objects.filter(purchaser=request.user)

    context = {
        'receipt_list': receipt_objects,
    }
    return render(request, 'receipts/receiptlist.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)

        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect('home')

    else:
        form = ReceiptForm()

    context = {
        'form': form,
    }

    return render(request, 'receipts/receiptcreate.html', context)

@login_required
def category(request):
    expense_category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'expense_category': expense_category,
    }
    return render(request, 'receipts/category.html', context)

@login_required
def accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        'accounts': accounts,
    }
    return render(request, 'receipts/accounts.html', context)

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseForm(request.POST)

        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect('category')

    else:
        form = ExpenseForm()

    context = {
        'form': form,
    }
    return render(request, 'receipts/addexpense.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)

        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('accounts')

    else:
        form = AccountForm()

    context = {
        'form': form,
    }
    return render(request, 'receipts/addaccount.html', context)
